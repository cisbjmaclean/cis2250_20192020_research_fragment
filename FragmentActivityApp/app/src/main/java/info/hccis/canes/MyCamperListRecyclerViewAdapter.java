package info.hccis.canes;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.hccis.canes.ui.CampersFragment.OnListFragmentInteractionListener;
import info.hccis.canes.entity.Camper;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Camper} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCamperListRecyclerViewAdapter extends RecyclerView.Adapter<MyCamperListRecyclerViewAdapter.ViewHolder> {

    private final List<Camper> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyCamperListRecyclerViewAdapter(List<Camper> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_campers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getId().toString());
        holder.mFirstNameView.setText(mValues.get(position).getFirstName());
        holder.mLastNameView.setText(mValues.get(position).getLastName());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mLastNameView;
        public final TextView mFirstNameView;
        public Camper mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mLastNameView = (TextView) view.findViewById(R.id.last_name);
            mFirstNameView = (TextView) view.findViewById(R.id.first_name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mLastNameView.toString() + "'";
        }
    }
}
