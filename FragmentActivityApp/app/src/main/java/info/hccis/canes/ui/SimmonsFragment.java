package info.hccis.canes.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.VideoView;

import info.hccis.canes.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SimmonsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SimmonsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public SimmonsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SimmonsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SimmonsFragment newInstance(String param1, String param2) {
        SimmonsFragment fragment = new SimmonsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


//
//

    }

    @Override
    public void onStart() {
        super.onStart();
        final SeekBar mySeekBar = getView().findViewById(R.id.seekBarTest);

        Log.d("bjm", "Hello");
        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int seekBarProgress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                seekBarProgress = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("bjm", "Tracking");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                toastMessage(seekBarProgress);
            }
        });


    }

    public VideoView videoView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_simmons, container, false);

        videoView = view.findViewById(R.id.videoView);
        MediaController mediacontroller = new MediaController(getContext());
        mediacontroller.setAnchorView(videoView);
        videoView.setMediaController(mediacontroller);
        videoView.setVideoURI(Uri.parse("https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"));
        videoView.requestFocus();

        return view;
    }

    public void toastMessage(int sliderValue) {

        String message = "";

        if (sliderValue < 5) {
            message = "Tiger";


        }
        if (sliderValue >= 5 && sliderValue <= 8) {
            message = "Lion";
        }
        if (sliderValue == 9 || sliderValue == 10) {

            message = "Wolf";

        }
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

    }

}
