package info.hccis.canes.ui.webster;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WebsterViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public WebsterViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a Webster fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
