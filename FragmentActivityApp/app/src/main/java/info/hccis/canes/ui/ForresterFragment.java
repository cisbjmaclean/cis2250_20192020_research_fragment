package info.hccis.canes.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import info.hccis.canes.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ForresterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForresterFragment extends Fragment implements SurfaceHolder.Callback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private WebView webView;
    private ProgressBar pbar;
    private CalendarView calendarView;
    private SurfaceView surfaceView;



    public ForresterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ForresterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForresterFragment newInstance(String param1, String param2) {
        ForresterFragment fragment = new ForresterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forrester, container, false);

        webView = view.findViewById(R.id.webView);
        pbar = view.findViewById(R.id.progressBar);
//        calendarView = view.findViewById(R.id.calendarView);
//        calendarView.setVisibility(View.GONE);

        surfaceView = view.findViewById(R.id.surfaceView2);
        surfaceView.getHolder().addCallback(this);
        surfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Canvas canvas = surfaceView.getHolder().lockCanvas();
                surfaceView.setRotation(50);

                canvas.drawColor(Color.BLUE);
//                surfaceView.draw(canvas);

                surfaceView.getHolder().unlockCanvasAndPost(canvas);
//                draw(surfaceView.getHolder());
            }
        });


        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://cforrester.netlify.com/");


        return view;
    }


    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        Canvas canvas = holder.lockCanvas();
        canvas.drawColor(Color.RED);
//        canvas.rotate(30f);
        surfaceView.draw(canvas);
        holder.unlockCanvasAndPost(canvas);


    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

//

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {


    }


    public class WebViewClient extends android.webkit.WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url){
            super.onPageFinished(view, url);
            pbar.setVisibility(View.GONE);
//            calendarView.setVisibility(View.VISIBLE);
        }
    }

}
