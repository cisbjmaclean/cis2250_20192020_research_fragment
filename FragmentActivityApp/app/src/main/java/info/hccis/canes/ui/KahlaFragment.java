package info.hccis.canes.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.canes.R;

public class KahlaFragment extends Fragment {
    Button submitButton;
    SeekBar customSeekBar;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public KahlaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MacLeanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KahlaFragment newInstance(String param1, String param2) {
        KahlaFragment fragment = new KahlaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kahla, container, false);

        //Rating Bar
        RatingBar ratingBar = view.findViewById(R.id.ratingBarKahla);
        ratingBar.setOnRatingBarChangeListener(new
                                                       RatingBar.OnRatingBarChangeListener() {
                                                           @Override
                                                           public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                                               Log.d("bjtest", "rating changed");
                                                               Snackbar.make(getView(), "rating changed " + ratingBar.getRating(), Snackbar.LENGTH_LONG)
                                                                       .setAction("Action", null).show();
                                                           }
                                                       });

        //Image View
        ImageView simpleImageViewLion = view.findViewById(R.id.simpleImageViewLion);//get the id of first image view
        simpleImageViewLion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "lion", Toast.LENGTH_LONG).show();//display the text on image click event
            }
        });

        //Progress Bar
        // initiate progress bar and start button
        final ProgressBar simpleProgressBar = (ProgressBar) view.findViewById(R.id.simpleProgressBar);
        Button startButton = (Button) view.findViewById(R.id.startButton);
        // perform click event on button
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // visible the progress bar
                simpleProgressBar.setVisibility(View.VISIBLE);
            }
        });

        //Seekbar
        // initiate views
        customSeekBar = (SeekBar) view.findViewById(R.id.customSeekBar);
        // perform seek bar change listener event used for getting the progress value
        customSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }

}

