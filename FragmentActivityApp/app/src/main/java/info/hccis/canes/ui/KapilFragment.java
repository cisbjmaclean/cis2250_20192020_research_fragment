package info.hccis.canes.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.canes.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link KapilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KapilFragment extends Fragment {

    CalendarView calendar;
    TextView dateView;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public KapilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment KapilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KapilFragment newInstance(String param1, String param2) {
        KapilFragment fragment = new KapilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kapil, container, false);

        RatingBar ratingBar = view.findViewById(R.id.ratingBarKapil);
        ratingBar.setOnRatingBarChangeListener(new
                                                       RatingBar.OnRatingBarChangeListener() {
                                                           @Override
                                                           public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                                               Log.d("bjtest", "rating changed");
                                                               Snackbar.make(getView(), "rating changed " + ratingBar.getRating(), Snackbar.LENGTH_LONG)
                                                                       .setAction("Action", null).show();
                                                           }
                                                       });
        calendar = view.findViewById(R.id.calendarViewKapil);
        dateView = view.findViewById(R.id.dateView);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String Date = dayOfMonth + "-" + (month + 1) + "-" + year;
                dateView.setText(Date);
            }
        });

        return view;


    }
}
