package info.hccis.canes.ui;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import info.hccis.canes.R;

public class BlanchardFragment extends Fragment {

    private BlanchardViewModel mViewModel;

    public static BlanchardFragment newInstance() {
        return new BlanchardFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blanchard, container, false);
        final CalendarView calender = (CalendarView) view.findViewById(R.id.calendarView);
        final TextView calendarOutput = (TextView) view.findViewById(R.id.calendarOutput);

        calender.setOnDateChangeListener( new CalendarView.OnDateChangeListener() {
                            @Override

                            // In this Listener have one method
                            // and in this method we will
                            // get the value of DAYS, MONTH, YEARS
                            public void onSelectedDayChange(
                                    @NonNull CalendarView view,
                                    int year,
                                    int month,
                                    int dayOfMonth)
                            {

                                String Date = "The date is " + dayOfMonth + "-" + (month + 1) + "-" + year;

                                calendarOutput.setText(Date);
                            }
                        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(BlanchardViewModel.class);
        // TODO: Use the ViewModel
    }

}
