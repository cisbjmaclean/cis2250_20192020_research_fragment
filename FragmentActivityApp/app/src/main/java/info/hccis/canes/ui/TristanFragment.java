package info.hccis.canes.ui;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import info.hccis.canes.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TristanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TristanFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TristanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TristanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TristanFragment newInstance(String param1, String param2) {
        TristanFragment fragment = new TristanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tristan, container, false);

        final TextView calendarText = (TextView) view.findViewById(R.id.textViewTristan);

        //Getting calenderView that we created in layout
        CalendarView calendar = (CalendarView) view.findViewById(R.id.calendarViewTristan);
        //Getting seekbar from layout created
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBarTristan);
        //Progress referencing progress bar
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarTristan);
        //This sets the listener to the seekBar
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //This simply updates the progress bar with the seek bar as it changes.
                progressBar.setProgress(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //This is what method gets called when there is a date change.
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                //Here we create the string in which gets displayed in the text view.
                String date = (month + 1) + "/" + (dayOfMonth) + "/" + (year);
                //This than sets the text of the calendar text.
                calendarText.setText(date);
            }
        });

        return view;
    }

}
