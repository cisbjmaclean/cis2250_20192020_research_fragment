package info.hccis.canes.util;
import info.hccis.canes.entity.Camper;
import retrofit2.Call;
import java.util.List;
import retrofit2.http.GET;

public interface JsonCamperApi {
    @GET("camper")
    Call<List<Camper>> getCampers();
}
