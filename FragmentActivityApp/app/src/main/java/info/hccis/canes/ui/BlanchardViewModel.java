package info.hccis.canes.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BlanchardViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public BlanchardViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a Blanchard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
