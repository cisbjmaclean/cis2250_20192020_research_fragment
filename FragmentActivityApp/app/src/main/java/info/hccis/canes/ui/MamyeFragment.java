package info.hccis.canes.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CalendarView;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import info.hccis.canes.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MamyeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MamyeFragment extends Fragment {

    private WebView wv1;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public MamyeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MamyeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MamyeFragment newInstance(String param1, String param2) {
        MamyeFragment fragment = new MamyeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View v = inflater.inflate(R.layout.fragment_mamye, container, false);

        wv1 = (WebView) v.findViewById(R.id.webView);
        wv1.loadUrl("https://stackoverflow.com/questions/31159149/using-webview-in-fragment/31159185");
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.setWebViewClient(new WebViewClient());

        RatingBar rb1 = v.findViewById(R.id.ratingBar);
        rb1.setOnRatingBarChangeListener(new
                                                 RatingBar.OnRatingBarChangeListener() {
                                                     @Override
                                                     public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                                                         Log.d("DSM1","rating has changed");

                                                         Toast.makeText(getActivity(),"rating is "+String.valueOf(ratingBar.getRating()), Toast.LENGTH_LONG).show();
                                                     }
                                                 }

        );

        CalendarView cv1 = (CalendarView) v.findViewById(R.id.calendarView);
        Long selectedDate = cv1.getDate();

        // Inflate the layout for this fragment
        return v;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
