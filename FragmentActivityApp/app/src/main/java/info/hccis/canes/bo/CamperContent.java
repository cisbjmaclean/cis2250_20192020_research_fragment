package info.hccis.canes.bo;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.canes.ui.CampersFragment;
import info.hccis.canes.MainActivity;
import info.hccis.canes.entity.Camper;
import info.hccis.canes.util.JsonCamperApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CamperContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Camper> CAMPERS = new ArrayList<Camper>();

    public static List<Camper> getCampersFromRoom(){

        Camper newCamper = new Camper();
        newCamper.setFirstName("Charlie");
        newCamper.setLastName(("Smith"));

        //Test the room add functionality
        try {
            MainActivity.myAppDatabase.camperDAO().add(newCamper);
        }catch(Exception e){
            MainActivity.myAppDatabase.camperDAO().update(newCamper);
        }
        Log.d("bjm", "Added camper");

        List<Camper> campersBack = MainActivity.myAppDatabase.camperDAO().get();
        Log.d("bjm", "loaded from room db, size="+campersBack.size());

        Log.d("bjm", "Here they are");
        for(Camper current: campersBack){
            Log.d("bjm", current.toString());
        }
        Log.d("bjm", "that's it");






        return null;


    }










    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the CAMPERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadCampers() {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Camper.CAMPER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCamperApi jsonCamperApi = retrofit.create(JsonCamperApi.class);

        Call<List<Camper>> call = jsonCamperApi.getCampers();

        call.enqueue(new Callback<List<Camper>>() {

            @Override
            public void onResponse(Call<List<Camper>> call, Response<List<Camper>> response) {
                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code" + response.code());
                    return;
                }
                List<Camper> campers = response.body();
                Log.d("bjm", "data back from service call #returned=" + campers.size());

                //**********************************************************************************
                // Now that we have the campers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                CamperContent.CAMPERS.clear();
                CamperContent.CAMPERS.addAll(campers);
                CampersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Camper>> call, Throwable t) {
//                Util.showDialog(, "continue?", "continue?");

                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());
            }
        });


    }


}
