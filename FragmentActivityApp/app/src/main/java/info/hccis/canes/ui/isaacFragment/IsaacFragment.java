package info.hccis.canes.ui.isaacFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import info.hccis.canes.R;

import static android.widget.Toast.LENGTH_LONG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link IsaacFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IsaacFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public IsaacFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IsaacFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IsaacFragment newInstance(String param1, String param2) {
        IsaacFragment fragment = new IsaacFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    Button button;
    ImageView image;
    ProgressBar progressBar;
    int tracker = 0;
    int progress = 0;
    boolean isReset;
    Context mContext;
    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    public void addListenerOnButton(View view) {

        progressBar = view.findViewById(R.id.progressBar3);
        image = view.findViewById(R.id.imageView3);
        button = view.findViewById(R.id.buttonChange);

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(isReset) {
                    button.setVisibility(View.INVISIBLE);
                    button.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            button.setVisibility(View.VISIBLE);
                        }
                    },1000);
                    button.setText("Change");
                    isReset=false;
                    progressBar.setProgress(0);
                    image.setImageResource(R.drawable.android_simple);
                }else {
                    progress += 10;
                    progressBar.setProgress(progress);
                    if (tracker == 0) {
                        tracker = 1;
                        image.setImageResource(R.drawable.android_3d);
                    } else {
                        tracker = 0;
                        image.setImageResource(R.drawable.android_simple);
                    }
                    if (progress >= 110) {
                        isReset = true;
                        button.setText("RESET");
                        progress = 0;
                        button.setVisibility(View.INVISIBLE);
                        button.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                            button.setVisibility(View.VISIBLE);
                            }
                        },3000);
                        Toast toast = Toast.makeText(getActivity().getApplicationContext(),"100% COMPLETE!!!", LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                }
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_isaac, container, false);
        image = view.findViewById(R.id.imageView3);
        image.setImageResource(R.drawable.android_simple);
        addListenerOnButton(view);
        return view;
    }

}