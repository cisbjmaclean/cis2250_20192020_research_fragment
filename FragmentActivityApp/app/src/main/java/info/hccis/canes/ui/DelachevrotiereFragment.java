package info.hccis.canes.ui;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import info.hccis.canes.R;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.Switch;

import com.google.android.material.snackbar.Snackbar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DelachevrotiereFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DelachevrotiereFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Switch switch_button;
    private CalendarView calendar;
    private RatingBar ratingBar;


    public DelachevrotiereFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DelachevrotiereFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DelachevrotiereFragment newInstance(String param1, String param2) {
        DelachevrotiereFragment fragment = new DelachevrotiereFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      /*  Switch switch_button;
        CalendarView calendar;

        switch_button = getView().findViewById(R.id.switch_hide_calender);
        calendar = getView().findViewById(R.id.calendarView);


        if(switch_button.isChecked()){
            Log.d("cdtest","Switch is on");
            calendar.setVisibility(View.VISIBLE);
        } else {
            calendar.setVisibility(View.INVISIBLE);
            Log.d("cdtest","Switch is off");
        }

       */

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delachevrotiere, container, false);

        /*

        CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener((new
                                                      CalendarView.OnDateChangeListener() {
                                                          @Override
                                                          public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                                                              String date = (i1 + 1) + "/" + i2 + "/" + i;
                                                              Log.d("cdtest","Date Changed: " + date);
                                                              Snackbar.make(getView(), "Date Changed" + date, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                          }
                                                      }));

         */

        return view;
    }

    public void onStart() {
        super.onStart();


        switch_button = getView().findViewById(R.id.switch_hide_calender);
        calendar = getView().findViewById(R.id.calendarView);
        ratingBar = getView().findViewById(R.id.ratingBar);

        //display date selected in snackbar and console on change
        calendar.setOnDateChangeListener((new
                                                  CalendarView.OnDateChangeListener() {
                                                      @Override
                                                      public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                                                          String date = (i1 + 1) + "/" + i2 + "/" + i;
                                                          Log.d("cdtest","Date Changed: " + date);
                                                          Snackbar.make(getView(), "Date Changed" + date, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                                                      }
                                                  }));
        //display rating in snackbar and console on change
        ratingBar.setOnRatingBarChangeListener(new
                                                       RatingBar.OnRatingBarChangeListener() {
                                                           @Override
                                                           public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                                                               Log.d("cdtest", "rating changed");
                                                               Snackbar.make(getView(), "rating changed " + ratingBar.getRating(), Snackbar.LENGTH_LONG)
                                                                       .setAction("Action", null).show();
                                                           }
                                                       });

        //swithc to hide or show calendar
        switch_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(switch_button.isChecked()){
            Log.d("cdtest","Switch is on");
            calendar.setVisibility(View.VISIBLE);
        } else {
            calendar.setVisibility(View.INVISIBLE);
            Log.d("cdtest","Switch is off");
        }
            }


        });

    }

}
