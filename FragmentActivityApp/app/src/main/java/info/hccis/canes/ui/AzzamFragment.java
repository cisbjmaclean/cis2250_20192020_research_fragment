package info.hccis.canes.ui;


import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Switch;

import androidx.fragment.app.Fragment;

import info.hccis.canes.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AzzamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AzzamFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RatingBar ratingBar;
    private Switch switch_theme;
    private Fragment fragment;
    private CalendarView calendarView;
    private SeekBar seekBar;
    private ProgressBar progressBar;

    public AzzamFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExampleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AzzamFragment newInstance(String param1, String param2) {
        AzzamFragment fragment = new AzzamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    /**
     *
     * author      asheikho
     * since       20200124
     * Description switch an image based on switch widget state (on/off)
     */
    @Override
    public void onStart() {
        super.onStart();

        switch_theme = getView().findViewById(R.id.switch_show_calender);
        calendarView = getView().findViewById(R.id.calendarView);
        progressBar = getView().findViewById(R.id.progressBar);
        seekBar = getView().findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progressChangedValue = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                progressBar.setProgress(progressChangedValue);
            }
        });

        switch_theme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // When turned on
                if(switch_theme.isChecked()){

                    Log.d("asheikho","Switch is clicked");
                    calendarView.setVisibility(View.VISIBLE);
                    // When turned off
                } else {
                    calendarView.setVisibility(View.INVISIBLE);
                    Log.d("asheikho","Switch is off");
                }
            }


        });

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_azzam, container, false);
    }

}
