package info.hccis.canes;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.view.Menu;
import android.widget.Toast;

import info.hccis.canes.bo.CamperContent;
import info.hccis.canes.dao.MyAppDatabase;
import info.hccis.canes.entity.Camper;
import info.hccis.canes.ui.CamperFragment;
import info.hccis.canes.ui.CampersFragment;

public class MainActivity extends AppCompatActivity implements CampersFragment.OnListFragmentInteractionListener, CamperFragment.OnFragmentInteractionListener {

    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;
    private static  NavController navController = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_example, R.id.nav_kylie, R.id.nav_jeremy, R.id.nav_azzam, R.id.nav_divya, R.id.nav_tristan, R.id.nav_robbie,R.id.nav_campers_list)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    public static NavController getNavController(){
        return navController;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * This method will be used in the camper fragment when the user clicks on a row of the
     * camper recyclerview.  This method will transfer the user to a details fragment.  The
     * id of the camper will be passed to the fragment and used to load the correct camper details.
     * Will use the arraylist associated with the recyclerview.
     * @param item the camper
     * @since 20200124
     * @author BJM
     */

    @Override
    public void onListFragmentInteraction(Camper item) {
        Log.d("bjm", "item communicated from fragment: "+item.toString());

        Toast.makeText(this, "item:"+item.getFirstName(), Toast.LENGTH_SHORT).show();

        getNavController().navigate(R.id.nav_camper);




    }


    @Override
    public void onFragmentInteraction() {

    }
}
